FROM ubuntu

WORKDIR /opt/

COPY ./CMakeLists.txt ./
COPY ./src/* ./src/
RUN apt update
RUN apt install -y libncurses6 cmake  
RUN apt install -y gcc-9
RUN apt install -y g++-9
RUN cmake . && cmake --build .

RUN pwd&&ls
