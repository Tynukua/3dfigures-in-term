#include <stdio.h>
#include "drawcall.h"

float A = 1, B = 1, C =1;

float drawFigure(float* buf, float x, float y, float R){
    float max = 0;
    for (int  i = 0;i<LINES;i++)
        for (int j = 0;j<COLS;j++){
            int bufi = (i*COLS+j);
            // distance from center to point
            float t = sqrtf(powf(((float)j-x), 2)+powf(i*2-y,2));

            if (t<=R) {
                buf[bufi] = R - t+1;
            }
            else buf[bufi] = 0;
            if (buf[bufi]> max)
                max = buf[bufi];
        }

    return max;
}


void drawcall(float * buf, float max){
    for (int  i = 0;i<LINES;i++){
        move(i, 0);
        for (int j = 0;j<COLS;j++){
            int bufi = (i*COLS+j);
            int N = buf[bufi]/(max/12);
            if (buf[bufi]==0){
                addch(' ');
            }
            else{
                int N = buf[bufi]/(max/11);
             
                addch(".,-~:;!*=#$@"[N>0?N:0]);
//              if(buf[bufi])
//                  addch('.');
            }
            buf[bufi]=0;
        }
    }
}

float drawSphere(float* buf, float x, float y, float R){
    float max = 0;
    for (int  i = 0;i<LINES;i++)
        for (int j = 0;j<COLS;j++){
            int bufi = (i*COLS+j);
            
            // distance from center to point
            float xi = j-x;
            float yi = i*2-y;
            float t = sqrtf(powf(xi, 2)+powf(yi,2));

            if (t<=R) {
                float z = sqrtf( R*R -t*t);
                buf[bufi] = (xi*A +  yi*B + z*C);
                if(buf[bufi]== 0)
                    buf[bufi] = -1;
            }
            if (buf[bufi]> max)
                max = buf[bufi];
        }

    return max;
}
