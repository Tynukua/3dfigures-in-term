#include <stdlib.h>
#include <unistd.h>

#include "drawcall.h"
extern float A,B,C;
int main(){
   WINDOW*mwin = initscr();
    noecho();
    curs_set(0);

    float*  f = NULL;
    int fx = -1, fy = -1;
    int a = 0;
    while(1){
        if(COLS != fx || LINES != fy){
            if(f) free(f);
            f = malloc(sizeof(float)*COLS*LINES);
            fx = COLS;
            fy = LINES;
        }

        A = sinf((float)a/30);
        C = cosf((float)a/20);
        if(C<0) C = -C;
        C = 0.5;
        if(A>0) A = -A;
        B = cosf((float)a/10);
        a++;
        drawcall(f, drawSphere(f, COLS/2+1,LINES,LINES-3));
        refresh();
        usleep(50000);
    }
    return 0;
    endwin();
}

