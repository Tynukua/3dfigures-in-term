#ifndef DRAWCALL_H
#define DRAWCALL_H
#include <curses.h>
#include <math.h>


float drawFigure(float* buf, float x, float y, float R);

float drawSphere(float* buf, float x, float y, float R);

void drawcall(float * buf, float max);

#endif
